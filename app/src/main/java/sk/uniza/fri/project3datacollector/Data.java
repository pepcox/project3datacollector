package sk.uniza.fri.project3datacollector;

/**
 * Created by Pepcox on 10/9/2016.
 */

public class Data {

    private float[] accelerometerData = {-1f, -1f, -1f};
    private float[] gyroData = {-1f, -1f, -1f};
    private String timeStamp = "";
    private double latitude = -1d;
    private double longitude = -1d;
    private float speed = -1f;

    public Data(float[] accelerometerData, float[] gyroData, String timeStamp, double latitude, double longitude, float speed) {
        this.accelerometerData[0] = accelerometerData[0];
        this.accelerometerData[1] = accelerometerData[1];
        this.accelerometerData[2] = accelerometerData[2];
        this.gyroData[0] = gyroData[0];
        this.gyroData[1] = gyroData[1];
        this.gyroData[2] = gyroData[2];
        this.timeStamp = timeStamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
    }

    public Data() {
    }

    public void setAccelerometerData(float[] accelerometerData) {
        this.accelerometerData[0] = accelerometerData[0];
        this.accelerometerData[1] = accelerometerData[1];
        this.accelerometerData[2] = accelerometerData[2];
    }

    public void setGyroData(float[] gyroData) {
        this.gyroData[0] = gyroData[0];
        this.gyroData[1] = gyroData[1];
        this.gyroData[2] = gyroData[2];
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float[] getAccelerometerData() {
        return accelerometerData;
    }

    public float[] getGyroData() {
        return gyroData;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getSpeed() {
        return speed;
    }

    public static String headerData() {
        return "dateTime;accelerometer[X];accelerometer[Y];accelerometer[Z];gyro[X];gyro[Y];gyro[Z];latitude;longitude;speed";
    }

    @Override
    public String toString() {
        return timeStamp + ";" +
                accelerometerData[0] + ";" +
                accelerometerData[1] + ";" +
                accelerometerData[2] + ";" +
                gyroData[0] + ";" +
                gyroData[1] + ";" +
                gyroData[2] + ";" +
                latitude + ";" +
                longitude + ";" +
                speed;
    }
}
