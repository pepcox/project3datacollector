package sk.uniza.fri.project3datacollector;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements SensorEventListener, LocationListener {

    @BindView(R.id.data_file_name)
    protected EditText fileName;
    @BindView(R.id.record_count_field)
    protected TextView recodCount;
    @BindView(R.id.delay_field)
    protected EditText delayField;
    @BindView(R.id.wait_for_gps_checkbox)
    protected CheckBox gpsCheckbox;
    @BindView(R.id.start_button)
    protected Button startButton;
    @BindView(R.id.stop_button)
    protected Button stopButton;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.coordinate_latitude)
    protected TextView latitudeField;
    @BindView(R.id.coordinate_longitude)
    protected TextView longitudeField;
    @BindView(R.id.coordinate_speed)
    protected TextView speedField;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor gyro;
    private LocationManager locationManager;
    private boolean isRecording = false;
    private int delay = 100;
    private LinkedList<Data> dataList;
    private Data actualData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataList = new LinkedList<>();
        actualData = new Data();
        ButterKnife.bind(this);
        setLayoutEnability();
        initLocationManager();
        initSensors();
        setSupportActionBar(toolbar);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    private void initSensors() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
    }

    @OnClick(R.id.start_button)
    public void onStartRecording() {
        isRecording = true;
        setLayoutEnability();
        try {
            delay = Integer.parseInt(delayField.getText().toString());
        } catch (ClassCastException ex) {
            ex.printStackTrace();
            Toast.makeText(this, "Failed to cast: " + delayField.getText().toString() + " to integer value. Default value remain 100ms", Toast.LENGTH_LONG).show();
        }
        OutputWriterTask task = new OutputWriterTask();
        task.execute();
    }

    @OnClick(R.id.stop_button)
    public void onStopRecording() {
        isRecording = false;
        setLayoutEnability();
    }

    public void setLayoutEnability() {
        fileName.setEnabled(!isRecording);
        gpsCheckbox.setEnabled(!isRecording);
        startButton.setEnabled(!isRecording);
        stopButton.setEnabled(isRecording);
    }

    private void writeToFile(LinkedList<Data> dataList) {
        String path = Environment.getExternalStorageDirectory() + "/project3data/" + getFileName();
        File file = new File(path);
        if (!file.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        BufferedOutputStream bos = null;
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fOut));
            bw.write(Data.headerData());
            bw.newLine();
            for (Data data : dataList) {
                bw.write(data.toString());
                bw.newLine();
            }
            bw.close();
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFileName() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH.mm.ss_dd.MM.yyyy");
        return fileName.getText().toString() + "_" + sdf.format(new Date()) + ".csv";
    }

    private String getTimeStamp() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH.mm.ss.SSSS_dd.MM.yyyy");
        return sdf.format(new Date());
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                actualData.setAccelerometerData(event.values);
                break;
            case Sensor.TYPE_GYROSCOPE:
                actualData.setGyroData(event.values);
                break;
        }
        //Log.d("nanana", event.values[0] + "   " + event.values[1] + "   " + event.values[2] + "   ");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void initLocationManager() {
        // Get the location manager
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, this);

    }

    @Override
    public void onLocationChanged(Location location) {
        actualData.setSpeed(location.getSpeed());
        actualData.setLatitude(location.getLatitude());
        actualData.setLongitude(location.getLongitude());
        latitudeField.setText(String.valueOf(actualData.getLatitude()));
        longitudeField.setText(String.valueOf(actualData.getLongitude()));
        speedField.setText(String.valueOf(actualData.getSpeed()));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class checkLocationTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }
    }

    private class OutputWriterTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (isRecording) {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            dataList.add(new Data(
                    actualData.getAccelerometerData(),
                    actualData.getGyroData(),
                    getTimeStamp(),
                    actualData.getLatitude(),
                    actualData.getLongitude(),
                    actualData.getSpeed()));
            Log.d("OutputWriterTask", "Created data: " + dataList.getLast().toString());
            recodCount.setText(String.valueOf(dataList.size()));

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            writeToFile(dataList);
            dataList.clear();
            recodCount.setText("Fucking zero");
            Toast.makeText(MainActivity.this, "Write to file was successfully executed", Toast.LENGTH_LONG).show();
        }
    }
}
